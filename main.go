package main

import (
	"cacoo/controllers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	// Init Router
	r := mux.NewRouter()

	// Route Handlers / Endpoints
	r.Handle("/", http.FileServer(http.Dir("./views/")))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	// Use Auth0 authentication to authenticate users before accessing the app
	r.Handle("/api/sheets", controllers.AuthMiddleware(controllers.GetSheets)).Methods("GET")

	log.Fatal(http.ListenAndServe(":3000", r))
}
