# cacoo

This App is a simple demonstration of Cacoo API (https://developer.nulab-inc.com/docs/cacoo/api/1/diagram/).
The idea is just to fetch a diagram and list the sheets in a web page. 

## Getting started
Please after installing all of the dependencies visit this URL: http://localhost:3000, and you should be able to see the login page.
For authentication I have used the Auth0 library. For this reason it's important to leave the port (3000) unchanged. 

## Knows Issues
 1. A lot of things are hardcoded that should be somehow taken out of the code. Like using an `.env` file
 2. `Auth0` library has some issues that should be addressed from their side. If that's not possible, the auth middleware has to be switched with an alternative one
 
## Next Steps
 1. Integrate a dependency manager
 2. Support more endpoints
 3. Make use of Go features like goroutines and channels to enhance the performance.
 4. There is a big room for UI to be improved
 