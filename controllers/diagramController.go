package controllers

import (
	"cacoo/templates"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"net/http"
)

// Diagram Struct (Model)
type Diagram struct {
	DiagramID string  `json:"diagramId"`
	URL       string  `json:"url"`
	ImageURL  string  `json:"imageUrl"`
	Title     string  `json:"title"`
	Sheets    []Sheet `json:"sheets"`
}

// Sheet Struct (Model)
type Sheet struct {
	UID      string `json:"uid"`
	URL      string `json:"url"`
	ImageURL string `json:"imageUrl"`
	Name     string `json:"name"`
	Width    uint16 `json:"width"`
	Height   uint16 `json:"height"`
}

// SheetsPage struct
type SheetsPage struct {
	DiagramName string
	DiagramURL  string
	Sheets      []Sheet
}

// GetSheets Get All Sheets
var GetSheets = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	// hardcoded for now/ ideally this should go to an env file
	resp, _ := http.Get("https://cacoo.com/api/v1/diagrams/1mJ1jksULQ4knx5F.json?apiKey=FRcG7XyTE5weUamuOwlJ")
	respBody, _ := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	diagram := &Diagram{}
	err := json.Unmarshal(respBody, diagram)

	if err != nil {
		panic(err)
	}

	diagramTemplate := templates.DIAGRAM
	p := SheetsPage{DiagramName: diagram.Title, DiagramURL: diagram.URL, Sheets: diagram.Sheets}
	t, _ := template.ParseFiles(diagramTemplate)
	t.Execute(w, p)
})
