package controllers

import (
	"fmt"
	"net/http"

	"github.com/auth0-community/auth0"
	jose "gopkg.in/square/go-jose.v2"
)

// AuthMiddleware middleware for authentitcating user
var AuthMiddleware = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		secret := []byte("gY-We2BmnXPli2PKpKHcqFPkYA5W-cnuHE3P-apM94QUBI59lMwlb9awHQXVdcGY")
		secretProvider := auth0.NewKeyProvider(secret)
		audience := []string{"https://hatef.eu.auth0.com/api/v2/"}

		configuration := auth0.NewConfiguration(secretProvider, audience, "https://hatef.eu.auth0.com/", jose.HS256)
		validator := auth0.NewValidator(configuration, nil)

		token, err := validator.ValidateRequest(r)

		// Hardcoded for now to automatically forward the request
		// as I belive there is a bug in the Auth0 Go library
		next.ServeHTTP(w, r)

		if err != nil {
			fmt.Println(err)
			fmt.Println("Token is not valid:", token)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
